</div>

<script>
    WebFontConfig = {
        google: {families: ['Roboto:400,700:latin,latin-ext']},
        // custom: {
        //     families: ['Bw Aleta No 10']
        // }
    };
</script>
<script src="js/libs/webfont.js" async defer></script>

<!--<script src="js/libs/svg4everybody.min.js"></script>-->
<!--<script>-->
<!--    svg4everybody();-->
<!--</script>-->

<script src="js/libs/jquery-1.12.4.min.js"></script>
<script src="js/scripts.js?v=1"></script>

</body>
</html>
