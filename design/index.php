<?php include 'start-page.php'; ?>
<?php include 'header.php'; ?>

    <main role="main">

        <section class="section">

            <div class="container">

                <h1>HTML Ipsum Presents</h1>

                <p>
                    <strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.
                    <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed,
                    <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                    <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

                <h2>Header Level 2</h2>

                <ol>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                    <li>Aliquam tincidunt mauris eu risus.</li>
                </ol>

                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
                </blockquote>

                <h3>Header Level 3</h3>

                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                    <li>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                        <ul>
                            <li>dolor sit amet, consectetuer</li>
                            <li>dolor sit amet</li>
                            <li>dolor sit amet, consectetuer</li>
                        </ul>
                    </li>
                    <li>Aliquam tincidunt mauris eu risus.</li>
                </ul>

                <ul class="list">
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                    <li>Aliquam tincidunt mauris eu risus.</li>
                </ul>

                <p>
                    <input type="submit" class="btn" value="Tlačítko">
                    <button class="btn">Tlačítko</button>
                    <a class="btn">Tlačítko</a>
                </p>
                <p>
                    <input type="submit" class="btn btn--success" value="Tlačítko">
                    <button class="btn btn--success">Tlačítko</button>
                    <a class="btn btn--success">Tlačítko</a>
                </p>
                <p>
                    <input type="submit" class="btn btn--danger" value="Tlačítko">
                    <button class="btn btn--danger">Tlačítko</button>
                    <a class="btn btn--danger">Tlačítko</a>
                </p>

                <div class="alert alert--success"><p>
                        <strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas.
                    </p></div>
                <div class="alert alert--info"><p>
                        <strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas.
                    </p></div>
                <div class="alert alert--danger"><p>
                        <strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas.
                    </p></div>

                <table class="table" cellpadding="0" cellspacing="0" border="1">
                    <tr>
                        <th>položka</th>
                        <th>položka</th>
                        <th>položka</th>
                        <th>položka</th>
                    </tr>
                    <tr>
                        <td>položka</td>
                        <td>položka</td>
                        <td>položka</td>
                        <td>položka</td>
                    </tr>
                    <tr>
                        <td>položka</td>
                        <td>položka</td>
                        <td>položka</td>
                        <td>položka</td>
                    </tr>
                </table>

                <form action="#" method="post">

                    <p>
                        <label for="name">Name:</label>
                        <input type="text" id="name" value="Value">
                        <input type="text2" id="name2" value="Value" class="is-success">
                        <input type="text3" id="name3" value="Value" class="is-danger">
                    </p>

                    <p>
                        <label for="email">Email:</label>
                        <input type="email" id="email" value="Value">
                        <input type="email" id="email2" value="Value" class="is-success">
                        <input type="email" id="email3" value="Value" class="is-danger">
                    </p>

                    <p>
                        <label for="password">Password:</label>
                        <input type="password" id="password" value="Value">
                        <input type="password" id="password2" value="Value" class="is-success">
                        <input type="password" id="password3" value="Value" class="is-danger">
                    </p>

                    <p>
                        <label for="message">Message:</label><br>
                        <textarea id="message" rows="4"
                                  cols="50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, reprehenderit adipisci, rem officiis ut quaerat alias illo.</textarea>
                    </p>

                    <p>
                        <input type="submit" value="Odeslat formulář" class="btn">
                    </p>

                </form>

                <p>
                    <span class="icon icon--icon-chevron-down-3" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-chevron-down-3"></use></svg>
                    </span>
                    <span class="icon icon--icon-chevron-down-3-color" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-chevron-down-3-color"></use></svg>
                    </span>

                    <span class="icon icon--icon-close" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-close"></use></svg>
                    </span>
                    <span class="icon icon--icon-close-color" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-close-color"></use></svg>
                    </span>

                    <span class="icon icon--icon-media-play" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-media-play"></use></svg>
                    </span>
                    <span class="icon icon--icon-media-play-color" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-media-play-color"></use></svg>
                    </span>

                    <span class="icon icon--icon-vlajka" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-vlajka"></use></svg>
                    </span>
                    <span class="icon icon--icon-vlajka-color" style="background: #000;width: 250px;">
                        <svg class="icon__svg"><use xlink:href="#icon-vlajka-color"></use></svg>
                    </span>

                  <span class="icon icon--icon-house" style="background: #000;width: 250px;">
                      <svg class="icon__svg"><use xlink:href="#icon-house"></use></svg>
                  </span>
                  <span class="icon icon--icon-house-color" style="background: #000;width: 250px;">
                      <svg class="icon__svg"><use xlink:href="#icon-house-color"></use></svg>
                  </span>


                </p>

                <div class="row">

                    <div class="col">
                        <div class="btn">.col</div>
                    </div>

                </div>

                <div class="row">

                    <div class="col col--2">
                        <div class="btn">.col.col--2</div>
                    </div>
                    <div class="col col--2">
                        <div class="btn">.col.col--2</div>
                    </div>

                </div>

                <div class="row">

                    <div class="col col--3">
                        <div class="btn">.col.col--3</div>
                    </div>
                    <div class="col col--3">
                        <div class="btn">.col.col--3</div>
                    </div>
                    <div class="col col--3">
                        <div class="btn">.col.col--3</div>
                    </div>

                </div>

                <div class="row">

                    <div class="col col--4">
                        <div class="btn">.col.col--4</div>
                    </div>
                    <div class="col col--4">
                        <div class="btn">.col.col--4</div>
                    </div>
                    <div class="col col--4">
                        <div class="btn">.col.col--4</div>
                    </div>
                    <div class="col col--4">
                        <div class="btn">.col.col--4</div>
                    </div>

                </div>

                <div class="row">

                    <div class="col col--5">
                        <div class="btn">.col.col--5</div>
                    </div>
                    <div class="col col--5">
                        <div class="btn">.col.col--5</div>
                    </div>
                    <div class="col col--5">
                        <div class="btn">.col.col--5</div>
                    </div>
                    <div class="col col--5">
                        <div class="btn">.col.col--5</div>
                    </div>
                    <div class="col col--5">
                        <div class="btn">.col.col--5</div>
                    </div>

                </div>

                <div class="row">

                    <div class="col col--6">
                        <div class="btn">.col.col--6</div>
                    </div>
                    <div class="col col--6">
                        <div class="btn">.col.col--6</div>
                    </div>
                    <div class="col col--6">
                        <div class="btn">.col.col--6</div>
                    </div>
                    <div class="col col--6">
                        <div class="btn">.col.col--6</div>
                    </div>
                    <div class="col col--6">
                        <div class="btn">.col.col--6</div>
                    </div>
                    <div class="col col--6">
                        <div class="btn">.col.col--6</div>
                    </div>

                </div>

            </div>

        </section>

    </main>

<?php include 'footer.php'; ?>
<?php include 'end-page.php'; ?>
