$(document).ready(function () {
  let time = 7;
  let $bar,
    $slick,
    isPause,
    tick,
    percentTime;
  $slick = $('#slick-1');
  $slick.slick({
    dots: true,
    pauseOnDotsHover: true,
    mobileFirst: true,
    infinite: true,
    arrows: true,
    fade: true,
    draggable: true,
  });
  $bar = $('.slider-progress .progress');
  $('.homepage-header--slider--text').on({
    mouseenter: function () {
      isPause = true;
    },
    mouseleave: function () {
      isPause = false;
    }
  });
  $slick.on('beforeChange', function(){
    startProgressbar();
  });
  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 10);
  }
  function interval() {
    if (isPause === false) {
      percentTime += 1 / (time + 0.1);
      $bar.css({
        width: percentTime + "%"
      });
      if (percentTime >= 100) {
        $slick.slick('slickNext');
        startProgressbar();
      }
    }
  }
  function resetProgressbar() {
    $bar.css({
      width: 0 + '%'
    });
    clearTimeout(tick);
  }
  startProgressbar();


  $('.reference--slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    appendDots: $(".reference--slider--dots"),
    arrows: true,
    prevArrow: $(".reference--slider--prev"),
    nextArrow: $(".reference--slider--next"),
    fade: false,
    autoplay: false,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });

  $('.blog-detail--editor--slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    fade: true,
    autoplay: false,
    autoplaySpeed: 5000,
  });

});