<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="description" content="...">

    <title>...</title>

    <meta property="og:title" itemprop="name" name="twitter:title" content="...">
    <meta property="og:url" name="twitter:url" content="...">
    <meta property="og:image" itemprop="image" name="twitter:image" content="...">
    <meta property="og:description" itemprop="description" name="twitter:description" content="...">

    <link rel="stylesheet" href="css/styles.css?v=1" type="text/css">
</head>

<body>

<div class="body-wrapper">